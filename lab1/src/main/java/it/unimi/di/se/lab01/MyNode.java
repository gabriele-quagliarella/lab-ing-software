package it.unimi.di.se.lab01;

public class MyNode {
    private double value;
    private MyNode next;

    public double getValue() {
        return this.value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public MyNode getNext() {
        return this.next;
    }

    public void setNext(MyNode next) {
        this.next = next;
    }

    public MyNode(double value) {
        this.value = value;
        this.next = null;
    }
}
