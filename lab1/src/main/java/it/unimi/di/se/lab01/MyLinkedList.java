package it.unimi.di.se.lab01;

import static java.lang.String.*;

public class MyLinkedList {

    private MyNode head;
    private double length;

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public MyLinkedList() {
        this.head = null;
        this.setLength(0);
    }

    public MyLinkedList(String args){
        String[] values = args.split(" ");
        this.setLength(0);

        for(int i=0; i<values.length; i++){
            try{
                this.addLast(Double.parseDouble(values[i]));
            }catch (Exception e){
                throw new IllegalArgumentException("Kaboom! Not supported input format.");
            }
        }
    }

    public String toString(){

        if (this.head == null)
            return "[]";
        else{
            String ListString = "[" + this.head.getValue();
            MyNode init = this.head.getNext();

            while (init != null && init.getNext() != null ){
                ListString = ListString + " " + valueOf(init.getValue());
                init = init.getNext();
            }

            if (init != null){
                ListString = ListString + " " + valueOf(init.getValue());
            }

            return ListString.concat("]");
        }
    }

    public void addLast(double n){
        if (this.head == null) {
            this.head = new MyNode(n);
        }
        else{
            MyNode init = head;

            while ( init.getNext() != null ){
                init = init.getNext();
            }

            init.setNext(new MyNode(n));
        }
        this.setLength(this.getLength() + 1);
    }

    public void addFirst(double n){
        MyNode newNode = new MyNode(n);
        newNode.setNext(this.head);
        this.head = newNode;
        this.setLength(this.getLength() + 1);
    }

    public boolean removeFirst(){
        if(this.head != null){
            this.head = this.head.getNext();
            this.setLength(this.getLength() - 1);
            return true;
        }else
            return false;
    }

    public boolean removeLast(){
        if(this.head != null){
            MyNode first = this.head;
            MyNode second = first.getNext();

            if (second != null) {
                while ( second != null && second.getNext() != null){
                    first = second;
                    second = second.getNext();
                }
                first.setNext(null);
            }else{
                this.head = null;
            }
            this.setLength(this.getLength() - 1);
            return true;
        }else
            return false;
    }

    public boolean remove(double n){
        if(this.head != null){
            MyNode first = null;
            MyNode second = this.head;

            while (second != null && second.getValue() != n){
                first = second;
                second = second.getNext();
            }

            if(second != null){
                if(second != this.head){
                    // In mezzo alla lista:
                    first.setNext(second.getNext());
                }else{
                    // L'elemento trovato è al primo posto:
                    this.head = this.head.getNext();
                }
                this.setLength(this.getLength() - 1);
                return true;
            }else{
                return false;
            }
        }else
            return false;
    }

    public double mean(){
        MyNode init = this.head;
        double sum = 0;

        while (init != null){
            sum += init.getValue();
            init = init.getNext();
        }

        if ( this.getLength() == 0) {
            return 0.0;
        }else{
            return (sum / this.getLength());
        }
    }
}
