package it.unimi.di.se.lab01;

import org.junit.Test;

import static org.assertj.core.api.Assertions.*;

public class Lab1Tests {

  private static final double EPSILON = 0.001;
  private MyLinkedList l;

  @Test
  public void testEmptyList() {
    l = new MyLinkedList();
    assertThat(l.toString()).isEqualTo("[]");
  }

  @Test
  public void testAddSingleElement() {
    l = new MyLinkedList();
    l.addLast(1);
    assertThat(l.toString()).isEqualTo("[1.0]");
  }

  @Test
  public void testAddMultipleElements() {
    l = new MyLinkedList();
    l.addLast(1);
    l.addLast(3.75);
    assertThat(l.toString()).isEqualTo("[1.0 3.75]");
  }

  @Test
  public void testNotEmptyConstructor() {
    l = new MyLinkedList("1");
    assertThat(l.toString()).isEqualTo("[1.0]");
  }

  @Test
  public void testNoEmptyConstructorWithMultipleElements() {
    l = new MyLinkedList("1 2 3");
    assertThat(l.toString()).isEqualTo("[1.0 2.0 3.0]");
  }

  @Test
  public void testFloatingElements() {
    l = new MyLinkedList("1.23 2.001 3.75");
    assertThat(l.toString()).isEqualTo("[1.23 2.001 3.75]");
  }

  @Test
  public void testIllegalArgumentInConstructor() {
    assertThatThrownBy(() -> {
      new MyLinkedList("1 2 abb");
    })
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("Kaboom! Not supported input format.");
  }


  @Test
  public void testManyValuesList() {
    l = new MyLinkedList();
    for (int i = 0; i < 100; i++) {
      l.addLast(i);
    }
    assertThat(l.toString()).startsWith("[0.0 1.0 2.0")
        .endsWith("98.0 99.0]");
  }

  @Test
  public void testAddToFirstPosition() {
    l = new MyLinkedList("1");
    l.addFirst(3.75);
    assertThat(l.toString()).isEqualTo("[3.75 1.0]");
    l.addFirst(1.25);
    assertThat(l.toString()).isEqualTo("[1.25 3.75 1.0]");
  }

  /*@Test
  public void testConstructorFromResourceFile() {
    InputStream resourceIS = getClass().getResourceAsStream("/input.txt");
    l = new MyLinkedList(resourceIS);
    assertThat(l.toString()).isEqualTo("[7.5 10.3 8.95 6.72 7.0 4.35 10.02]");
    assertThat(l.mean()).isCloseTo(7.834, within(EPSILON));
    assertThat(l.stdDev()).isCloseTo(2.093, within(EPSILON));
  }*/

  @Test
  public void testRemoveFirstElement() {
    l = new MyLinkedList("1 2");
    assertThat(l.removeFirst()).isTrue();
    assertThat(l.toString()).isEqualTo("[2.0]");
    assertThat(l.removeFirst()).isTrue();
    assertThat(l.toString()).isEqualTo("[]");
    assertThat(l.removeFirst()).isFalse();
    assertThat(l.toString()).isEqualTo("[]");
  }

  @Test
  public void testRemoveLastElement() {
    l = new MyLinkedList("1 2 3");
    assertThat(l.removeLast()).isTrue();
    assertThat(l.toString()).isEqualTo("[1.0 2.0]");
    assertThat(l.removeLast()).isTrue();
    assertThat(l.toString()).isEqualTo("[1.0]");
    assertThat(l.removeLast()).isTrue();
    assertThat(l.toString()).isEqualTo("[]");
    assertThat(l.removeLast()).isFalse();
    assertThat(l.toString()).isEqualTo("[]");
  }

  @Test
  public void testRemoveElementWithValue() {
    l = new MyLinkedList("1 2.5 3 3 4 5 4");
    assertThat(l.toString()).isEqualTo("[1.0 2.5 3.0 3.0 4.0 5.0 4.0]");
    assertThat(l.remove(2.5)).isTrue();
    assertThat(l.toString()).isEqualTo("[1.0 3.0 3.0 4.0 5.0 4.0]");
    assertThat(l.remove(1)).isTrue();
    assertThat(l.toString()).isEqualTo("[3.0 3.0 4.0 5.0 4.0]");
    assertThat(l.remove(4)).isTrue();
    assertThat(l.toString()).isEqualTo("[3.0 3.0 5.0 4.0]");
    assertThat(l.remove(25)).isFalse();
    assertThat(l.toString()).isEqualTo("[3.0 3.0 5.0 4.0]");
    l = new MyLinkedList();
    assertThat(l.remove(1)).isFalse();
  }

  @Test
  public void testAverageComputation() {
    MyLinkedList l = new MyLinkedList();
    MyLinkedList l1 = new MyLinkedList("1 2");
    MyLinkedList l2 = new MyLinkedList("160 591 114 229 230 270 128 1657 624 1503");
    MyLinkedList l3 = new MyLinkedList("15.0 69.9 6.5 22.4 28.4 65.9 19.4 198.7 38.8 138.2");

    assertThat(l.mean()).isCloseTo(0.0, within(EPSILON));
    assertThat(l1.mean()).isCloseTo(1.5, within(EPSILON));
    assertThat(l2.mean()).isCloseTo(550.6, within(EPSILON));
    assertThat(l3.mean()).isCloseTo(60.32, within(EPSILON));
  }

/*  @Test
  public void testStdDeviationComputation() {
    l = new MyLinkedList();
    //la funzione stdDev calcola la deviazione standard (https://it.wikipedia.org/wiki/Scarto_tipo#Deviazione_standard_corretta)
    assertThat(l.stdDev()).isCloseTo(0.0, within(EPSILON));
    l.addLast(1);
    assertThat(l.stdDev()).isCloseTo(0.0, within(EPSILON));
    l = new MyLinkedList("160 591 114 229 230 270 128 1657 624 1503");
    assertThat(l.stdDev()).isCloseTo(572.026, within(EPSILON));
    l = new MyLinkedList("15.0 69.9 6.5 22.4 28.4 65.9 19.4 198.7 38.8 138.2");
    assertThat(l.stdDev()).isCloseTo(62.255, within(EPSILON));
  }
  /**/
}
