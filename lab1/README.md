# Corso di Ingegneria del Software a.a. 2017/18

## Laboratorio 1

### Processo

Una volta importato il progetto, la coppia implementa secondo la metodologia **TDD** le specifiche riportate di seguito; in maggior dettaglio, ripete i
passi seguenti fino ad aver implementato tutte le funzionalità richieste:

* ripetere fino ad esaurimento dei test:
    * scommentare un nuovo test, 
    * [ROSSO] aggiungere, se necessario, il codice utile ad eliminare errori di compilazione,
    * [VERDE] aggiungere il codice necessario a far eseguire il test senza che vengano evidenziati errori e malfunzionamenti,
    * [REFACTORING] procedere, se necessario, al *refactoring* del codice per *migliorare* la soluzione funzionante.
    
La coppia lavora su UN SOLO computer. Ad ogni ciclo [Refactoring-Rosso-Verde] ci si scambia di posizione (cioè si cambia chi ha il possesso della tastiera).

### Specifica dei requisiti

Scopo dell'esercitazione è creare un programma Java che implementi una [linked list](https://en.wikipedia.org/wiki/Linked_list).
Nello specifico, i requisiti funzionali vengono forniti attraverso una insieme di test di unità presenti nel file [Lab1Test.java](src/test/java/it/uniupo/disit/se/lab01/Lab1Tests.java).
La coppia procede considerando un singolo test alla volta, come descritto nella sezione *Processo*.

Il software implementato dovrà essere **corretto** rispetto alla specifica dei requisiti, e **manutenibile**.
In questo senso la coppia dovrà adottare uno stile di programmazione orientato agli oggetti usando le principali convenzioni della pragrammazione Java.
Inoltre, prestare attenzione ad evitare *codice duplicato* e *poco leggibile*.

La *leggibilità* e la *manutenibilità* del software sono attributi di qualità di cui il gruppo deve occuparsi durante la fase di *refactoring*.
